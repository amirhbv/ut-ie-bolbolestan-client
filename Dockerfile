# Build Environment
FROM node:12-alpine as builder

RUN mkdir /app

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH
ENV NODE_ENV 'production'

COPY package.json ./
COPY yarn.lock ./

RUN yarn

COPY . ./

RUN yarn build

# Production Environment
FROM nginx:stable-alpine

COPY ./.nginx/nginx.conf /etc/nginx/nginx.conf

COPY --from=builder /app/build /usr/share/nginx/html

EXPOSE 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]
