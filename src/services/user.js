import axios from './base';
import { getToken } from '../app/hooks/use-auth';

export function signupUser(data) {
    return axios('/signup', {
        method: 'POST',
        data,
    });
}

export function loginUser(email, password) {
    return axios('/login', {
        method: 'POST',
        data: {
            email,
            password,
        },
    });
}

export function forgetPasswordUser(email) {
    return axios('/forget-password', {
        method: 'POST',
        data: {
            email,
        },
    });
}

export function changePasswordUser(password) {
    return axios('/api/change-password', {
        method: 'POST',
        headers: {
            'Authorization': getToken(),
        },
        data: {
            password,
        },
    });
}

export function getUserInfo() {
    return axios('/api/profile',{
        method: 'GET',
        headers: {
            'Authorization': getToken(),
        },
    });
}

export function getTermsReport() {
    return axios('/api/taken-courses',{
        method: 'GET',
        headers: {
            'Authorization': getToken(),
        },
    });
}
