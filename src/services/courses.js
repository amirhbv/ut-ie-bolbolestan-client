import axios from './base';
import { getToken } from '../app/hooks/use-auth';

export function getWeeklySchedule(submitted = false) {
    return axios('/api/weekly-schedule', {
        method: 'GET',
        params: {
            submitted,
        },
        headers: {
            'Authorization': getToken(),
        },
    });
}

export function getCourses(params) {
    return axios('/api/offerings', {
        method: 'GET',
        params,
        headers: {
            'Authorization': getToken(),
        },
    });
}

export function addToWeeklySchedule(code) {
    return axios('/api/weekly-schedule', {
        method: 'POST',
        headers: {
            'Authorization': getToken(),
        },
        data: {
            code,
        },
    });
}

export function addToWaitList(code) {
    return axios('/api/weekly-schedule/wait-list', {
        method: 'POST',
        headers: {
            'Authorization': getToken(),
        },
        data: {
            code,
        },
    });
}

export function submitWeeklySchedule() {
    return axios('/api/weekly-schedule/submit', {
        method: 'POST',
        headers: {
            'Authorization': getToken(),
        },
    });
}

export function removeFromWeeklySchedule(code) {
    return axios(`/api/weekly-schedule/${code}`, {
        method: 'DELETE',
        headers: {
            'Authorization': getToken(),
        },
    });
}

export function removeFromWaitList(code) {
    return axios(`/api/weekly-schedule/wait-list/${code}`, {
        method: 'DELETE',
        headers: {
            'Authorization': getToken(),
        },
    });
}
