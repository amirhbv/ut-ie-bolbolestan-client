import React, { useEffect, useState } from 'react';

import './Home.css';
import UserProfile from './UserProfile';
import TermReport from './TermReport';
import { getUserInfo, getTermsReport } from '../../services';

export default function Home() {
    document.title = 'بلبلستان | خانه';

    const [user, setUser] = useState({});
    const [termsReport, setTermsReport] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
    }, []);

    useEffect(() => {
        if (isLoading) {
            setTimeout(() => {
                Promise.all([
                    getUserInfo().then((resp) => setUser(resp.data)),
                    getTermsReport().then((resp) => setTermsReport(Object.entries(resp.data).map(([termNumber, grades]) => ({ termNumber, grades })))),
                ]).then(() => setIsLoading(false));
            }, 2000);
        }
    }, [isLoading]);

    return (
        <div className="container">
            {
                isLoading
                    ? (
                        <div className="d-flex justify-content-center p-5">
                            <div className="spinner-border" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>
                        </div>
                    ) : (
                        <div className="row">
                            <div className="col-3">
                                <UserProfile user={user} />
                            </div>

                            <div className="col px-4 py-5 terms-report">
                                {termsReport.map((termReport) => <TermReport key={termReport.termNumber} report={termReport}/>)}
                            </div>
                        </div>

                    )
            }
        </div>
    );
}
