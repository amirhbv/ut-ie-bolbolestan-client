import React from 'react';
import PropTypes from 'prop-types';

import './TermReport.css';
import Box from '../components/Box';

function TermReportRow({ gradeRow, index }) {
    const status = gradeRow.status;

    return (
        <tr className={`grades-table-row grades-table-row-${status}`}>
            <td className="grades-table-index">{index + 1}</td>
            <td className="grades-table-code">{gradeRow.code}</td>
            <td className="grades-table-name">{gradeRow.name}</td>
            <td className="grades-table-units">{gradeRow.units} واحد</td>
            <td className="grades-table-status">
                <span>{
                    {
                        pending: 'نامشخص',
                        passed: 'قبول',
                        failed: 'مردود',
                    }[status]
                }</span>
            </td>
            <td className="grades-table-grade">{gradeRow.grade || '-'}</td>
        </tr>
    );
}

TermReportRow.propTypes = {
    gradeRow: PropTypes.object,
    index: PropTypes.number,
};

function TermReport({ report }) {
    console.log('report', report);
    const { sumOfGrades, numberOfUnits } = report.grades.reduce((acc, curr) => {
        if (curr.grade) {
            acc.sumOfGrades += curr.grade * curr.units;
            acc.numberOfUnits += curr.units;
        }
        return acc;
    }, {
        sumOfGrades: 0,
        numberOfUnits: 0,
    });

    const gpa = sumOfGrades / numberOfUnits;

    return (
        <Box title={`کارنامه - ترم ${report.termNumber}`}>
            <table className="grades-table">
                <tbody>
                    {report.grades.map((gradeRow, index) => <TermReportRow key={gradeRow.code} gradeRow={gradeRow} index={index} />)}
                </tbody>
            </table>

            <div className="term-report-average-row">
                <span className="term-report-average">
                    معدل: {gpa.toFixed(2)}
                </span>
            </div>
        </Box>
    );
}

TermReport.propTypes = {
    report: PropTypes.object,
};

export default TermReport;
