import React from 'react';
import PropTypes from 'prop-types';

import './UserProfile.css';

function UserProfile({ user }) {

    return (
        <div className="profile">
            <img
                src={user.img}
                alt="user"
                width="120"
                height="120"
                className="d-inline-block"
            />
            <ul className="list-unstyled">
                <li>
                    <span className="profile-key">نام:  </span>
                    <span className="profile-value">{user.name + ' ' + user.lastName}</span>
                </li>
                <li>
                    <span className="profile-key">شماره دانشجویی: </span>
                    <span className="profile-value">{user.stdId}</span>
                </li>
                <li>
                    <span className="profile-key">تاریخ تولد: </span>
                    <span className="profile-value">{user.birthDate}</span>
                </li>
                <li>
                    <span className="profile-key">معدل کل: </span>
                    <span className="profile-value">{user.gpa}</span>
                </li>
                <li>
                    <span className="profile-key">واحد گذرانده: </span>
                    <span className="profile-value">{user.tpu}</span>
                </li>
                <li>
                    <span className="profile-key">دانشکده: </span>
                    <span className="profile-value">{user.faculty}</span>
                </li>
                <li>
                    <span className="profile-key">رشته: </span>
                    <span className="profile-value">{user.field}</span>
                </li>
                <li>
                    <span className="profile-key">مقطع: </span>
                    <span className="profile-value">{user.level}</span>
                </li>
                <li>
                    <div className="profile-state">
                        {user.status}
                    </div>
                </li>
            </ul>
        </div>
    );
}

UserProfile.propTypes = {
    user: PropTypes.object,
};

export default UserProfile;
