import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import './Schedule.css';
import Box from '../components/Box';
import { getWeeklySchedule } from '../../services/';

function ScheduleTable({ weeklySchedule }) {
    const tableRows = {};
    const startTimes = [];
    for (let i = 7; i < 18; i++) {
        startTimes.push(i);
        tableRows[i] = {};
    }
    const weekdays = ['saturday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday'];

    for (const course of weeklySchedule) {
        const startTime = course.start.split(':')[0];
        course.durationClassName = course.duration == 1.5 ? '1-5' : course.duration;
        tableRows[startTime][course.day] = course;
    }

    console.log(tableRows);

    function formatTime(time) {
        return `${time}:00 - ${time + 1}:00`;
    }

    return (
        <div className="schedule-table-container">
            <table className="schedule-table">
                <tbody>
                    <tr className="schedule-table-row">
                        <th></th>
                        <th>شنبه</th>
                        <th>یک‌شنبه</th>
                        <th>دوشنبه</th>
                        <th>سه‌شنبه</th>
                        <th>چهارشنبه</th>
                        <th>پنج‌شنبه</th>
                    </tr>

                    {
                        startTimes.map((time) => (
                            <tr key={time} className="schedule-table-row">
                                <td>{formatTime(time)}</td>
                                {
                                    weekdays.map((weekday) => (
                                        <td key={`${time}-${weekday}`}>
                                            {
                                                tableRows[time][weekday] ?
                                                    <div className={`schedule-time course-type-${tableRows[time][weekday].type} schedule-time-${tableRows[time][weekday].durationClassName} ${tableRows[time][weekday].withOffset?'schedule-time-offset-5':''}`}>
                                                        {tableRows[time][weekday].start} - {tableRows[time][weekday].end} <br />
                                                        {tableRows[time][weekday].name} <br />
                                                        {{
                                                            'asli': 'اصلی',
                                                            'umumi': 'عمومی',
                                                            'takhasosi': 'تخصصی',
                                                            'paaye': 'پایه',
                                                        }[tableRows[time][weekday].type]}
                                                    </div>
                                                    : ''
                                            }
                                        </td>
                                    ))
                                }
                            </tr>
                        ))
                    }

                </tbody>
            </table>
        </div>
    );
}

ScheduleTable.propTypes = {
    weeklySchedule: PropTypes.array,
};

export default function Schedule() {
    document.title = 'بلبلستان | برنامه هفتگی';

    const [weeklySchedule, setWeeklySchedule] = useState([]);

    function getWeeklyScheduleData() {
        return getWeeklySchedule(true).then((resp) => {
            const data = resp.data;
            console.log('getWeeklySchedule', data);
            const scheduledCourses = [];

            for (const course of data.submitted) {
                const classTime = course.classTime;
                classTime.days = classTime.days[0].split(',');
                for (const day of classTime.days) {
                    const time = classTime.time;

                    const start = time.start;
                    const end = time.end;

                    const [startHour, startMinute] = start.split(':');
                    const startNum = parseInt(startHour) + (startMinute == 30 ? .5 : 0);

                    const [endHour, endMinute] = end.split(':');
                    const endNum = parseInt(endHour) + (endMinute == 30 ? .5 : 0);

                    const duration = endNum - startNum;

                    scheduledCourses.push({
                        day,
                        start,
                        end,
                        duration,
                        withOffset: startMinute == 30,
                        type: course.type.toLowerCase(),
                        name: course.name,
                    });
                }
            }

            setWeeklySchedule(scheduledCourses);
        });
    }

    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
    }, []);

    useEffect(() => {
        if (isLoading) {
            setTimeout(() => {
                Promise.all([
                    getWeeklyScheduleData(),
                ]).then(() => setIsLoading(false));
            }, 2000);
        }
    }, [isLoading]);

    return (
        <div className="container">

            {
                isLoading
                    ? (
                        <div className="d-flex justify-content-center p-5">
                            <div className="spinner-border" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>
                        </div>
                    ) : (
                        <div className="row">
                            <div className="col px-4 pt-5">
                                <Box>
                                    <div className="schedule-header">
                                        <span>
                                            <i className="flaticon-calendar"></i>
                                برنامه هفتگی
                                        </span>
                                        {/* <span>ترم ۶</span> */}
                                    </div>

                                    {<ScheduleTable weeklySchedule={weeklySchedule} />}
                                </Box>

                            </div>
                        </div>
                    )
            }
        </div>
    );
}
