import React from 'react';
import './Footer.css';

export default function Footer() {

    return (
        <footer className="footer mt-auto pt-3 pb-1">
            <div className="container d-flex justify-content-between">
                <div>
                    <i className="flaticon-copyright footer-copyright-icon"></i>
                    دانشگاه تهران - سامانه جامع بلبلستان
                </div>

                <div>
                    <i className="footer-contact-icon flaticon-facebook"></i>
                    <i className="footer-contact-icon flaticon-linkedin-logo"></i>
                    <i className="footer-contact-icon flaticon-instagram"></i>
                    <i
                        className="footer-contact-icon flaticon-twitter-logo-on-black-background"
                    ></i>
                </div>
            </div>
        </footer>
    );

}
