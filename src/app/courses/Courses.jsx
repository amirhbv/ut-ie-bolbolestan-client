import React, { Fragment, useEffect, useState } from 'react';
import { toast } from 'react-toastify';
// import { Popover } from 'react-tiny-popover';


import './Courses.css';
import Box from '../components/Box';
import Input from '../components/Input';
import {
    getWeeklySchedule,
    getCourses,
    addToWaitList,
    addToWeeklySchedule,
    submitWeeklySchedule,
    removeFromWeeklySchedule,
    removeFromWaitList,
} from '../../services';


export default function Courses() {
    document.title = 'بلبلستان | انتخاب واحد';

    const [weeklySchedule, setWeeklySchedule] = useState([]);

    function getWeeklyScheduleData() {
        return getWeeklySchedule().then((resp) => {
            const data = resp.data;
            console.log('getWeeklySchedule', data);
            setWeeklySchedule([...data.submitted, ...(data.waitList.map((course) => ({...course, status: 'queued'})))]);
        });
    }

    const [courses, setCourses] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');
    const [courseTypeQuery, setCourseTypeQuery] = useState('');
    const courseTypes = [
        ['', 'همه'],
        ['Takhasosi', 'تخصصی'],
        ['Asli', 'اصلی'],
        ['Paaye', 'پایه'],
        ['Umumi', 'عمومی'],
    ];

    function getCoursesData() {
        const params = {};
        if (searchQuery) {
            params.q = searchQuery;
        }
        if (courseTypeQuery) {
            params.type = courseTypeQuery;
        }

        console.log('params', params);

        return getCourses(params).then((resp) => {
            const data = resp.data;
            console.log('getCourses', data);
            setCourses(data.map((course) => ({
                ...course,
                type: course.type.toLowerCase(),
                isFull: course.signedUpCount >= course.capacity,
            })));
        });
    }

    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
    }, []);

    useEffect(() => {
        if (isLoading) {
            setTimeout(() => {
                Promise.all([
                    getWeeklyScheduleData(),
                    getCoursesData(),
                ]).then(() => setIsLoading(false));
            }, 2000);
        }
    }, [isLoading]);

    useEffect(getCoursesData, [courseTypeQuery]);

    function addCourse(course) {
        const add = course.isFull ? addToWaitList : addToWeeklySchedule;

        add(course.uniqueCode).then(getWeeklyScheduleData).catch((err) => toast(err.response.data.error));
    }

    function submitUserWeeklySchedule() {
        submitWeeklySchedule().then(getWeeklyScheduleData).catch((err) => toast(err.response.data.error));
    }

    function removeCourse(course) {
        const remove = course.status === 'queued' ? removeFromWaitList : removeFromWeeklySchedule;

        remove(course.code).then(getWeeklyScheduleData).catch((err) => toast(err.response.data.error));
    }

    return (
        <div className="container">
            {
                isLoading
                    ? (
                        <div className="d-flex justify-content-center p-5">
                            <div className="spinner-border" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>
                        </div>
                    ) : (
                        <Fragment>

                            <div className="row">
                                <div className="col px-4 pt-5">
                                    <Box title="دروس انتخاب شده">
                                        <div className="selected-courses-table-container">
                                            <table className="selected-courses-table">
                                                <tbody>
                                                    <tr className="selected-courses-table-row">
                                                        <th className="selected-courses-table-remove"></th>
                                                        <th className="selected-courses-table-state">وضعیت</th>
                                                        <th className="selected-courses-table-code">کد</th>
                                                        <th className="selected-courses-table-name">نام درس</th>
                                                        <th className="selected-courses-table-instructor">استاد</th>
                                                        <th className="selected-courses-table-units">واحد</th>
                                                    </tr>

                                                    {
                                                        weeklySchedule.map((course) => (
                                                            <tr key={course.code} className={`selected-courses-table-row selected-courses-table-row-${course.status}`}>
                                                                <td className="selected-courses-table-remove" onClick={() => removeCourse(course)}>
                                                                    <i className="flaticon-trash-bin"></i>
                                                                </td>
                                                                <td className="selected-courses-table-status">
                                                                    <span>{{
                                                                        'pending': 'ثبت‌ نهایی نشده',
                                                                        'finalized': 'ثبت‌ شده',
                                                                        'queued': 'در انتظار',
                                                                        'toBeRemoved': 'حذف',
                                                                    }[course.status]}</span>
                                                                </td>
                                                                <td className="selected-courses-table-code">{course.code}</td>
                                                                <td className="selected-courses-table-name">{course.name}</td>
                                                                <td className="selected-courses-table-instructor">{course.instructor}</td>
                                                                <td className="selected-courses-table-units">{course.units}</td>
                                                            </tr>
                                                        ))
                                                    }
                                                </tbody>
                                            </table>
                                        </div>
                                        <div className="selected-courses-footer">
                                            <span className="selected-courses-units">
                                                تعداد واحد ثبت‌ شده: {weeklySchedule.filter((course) => course.status == 'finalized').reduce((acc, curr) => acc + curr.units, 0)}
                                            </span>
                                            <span className="selected-courses-buttons">
                                                <span className="selected-courses-refresh" onClick={getWeeklyScheduleData}>
                                                    <i className="flaticon-refresh-arrow"></i>
                                                </span>
                                                <span className="selected-courses-submit" onClick={submitUserWeeklySchedule}>
                                                    ثبت‌ نهایی
                                                </span>
                                            </span>
                                        </div>
                                    </Box>
                                </div>
                            </div>

                            <div className="d-flex justify-content-center pt-4">
                                <div className="search-container">
                                    <Input
                                        className="search-input"
                                        placeholder="نام درس"
                                        value={searchQuery}
                                        onChange={setSearchQuery}
                                    />
                                    <span className="search-button" onClick={getCoursesData}>
                                        جستجو
                                        <i className="flaticon-loupe"></i>
                                    </span>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col px-4 pt-4">
                                    <Box title="دروس ارائه شده">
                                        <div className="course-type-filters-container">
                                            {
                                                courseTypes.map(([type, name]) => (
                                                    <span
                                                        key={type}
                                                        className={`course-type-filter ${courseTypeQuery == type ? 'active' : ''}`}
                                                        onClick={() => setCourseTypeQuery(type)}
                                                    >
                                                        {name}
                                                    </span>
                                                ))
                                            }
                                        </div>

                                        <div className="available-courses-table-container">
                                            <table className="available-courses-table">
                                                <tbody>
                                                    <tr className="available-courses-table-row">
                                                        <th className="available-courses-table-actions"></th>
                                                        <th className="available-courses-table-code">کد</th>
                                                        <th className="available-courses-table-capacity">ظرفیت</th>
                                                        <th className="available-courses-table-type">نوع</th>
                                                        <th className="available-courses-table-name">نام درس</th>
                                                        <th className="available-courses-table-instructor">استاد</th>
                                                        <th className="available-courses-table-units">واحد</th>
                                                        <th className="available-courses-table-description">توضیحات</th>
                                                    </tr>

                                                    {
                                                        courses.map((course) => (
                                                            <tr key={course.uniqueCode} className={`available-courses-table-row available-courses-table-row-${course.type}`}>
                                                                <td
                                                                    className={`available-courses-table-actions available-courses-table-actions-${course.isFull ? 'queue' : 'add'}`}
                                                                    onClick={() => addCourse(course)}
                                                                >
                                                                    <i className={course.isFull ? 'flaticon-clock-circular-outline' : 'flaticon-add'}></i>
                                                                </td>
                                                                <td className="available-courses-table-code">{course.uniqueCode}</td>
                                                                <td className={`available-courses-table-capacity ${course.signedUpCount < course.capacity ? 'not-full' : ''}`}>{course.signedUpCount}/{course.capacity}</td>
                                                                <td className="available-courses-table-type">
                                                                    <span>{{
                                                                        'asli': 'اصلی',
                                                                        'umumi': 'عمومی',
                                                                        'takhasosi': 'تخصصی',
                                                                        'paaye': 'پایه',
                                                                    }[course.type]}</span>
                                                                </td>
                                                                <td className="available-courses-table-name">{course.name}</td>
                                                                <td className="available-courses-table-instructor">{course.instructor}</td>
                                                                <td className="available-courses-table-units">{course.units}</td>
                                                                <td className="available-courses-table-description">
                                                                    {/* <Popover
                                                                        isOpen={true}
                                                                        positions='left'
                                                                        content={<div>{course.prerequisites.join(' - ')}</div>}
                                                                    >
                                                                        <div></div>
                                                                    </Popover> */}
                                                                </td>
                                                            </tr>
                                                        ))
                                                    }
                                                </tbody>
                                            </table>
                                        </div>
                                    </Box>
                                </div>
                            </div>


                        </Fragment>
                    )
            }
        </div>
    );
}
