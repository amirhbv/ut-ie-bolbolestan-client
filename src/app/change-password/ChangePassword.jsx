import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { toast } from 'react-toastify';

import './ChangePassword.css';
import { useAuth } from '../hooks/use-auth';
import Input from '../components/Input';

export default function ChangePassword() {
    const [password, setPassword] = useState('');

    const history = useHistory();
    const auth = useAuth();

    function changePassword(e) {
        e.preventDefault();

        auth.changePassword(password).then(() => {
            toast.success('رمز عبور با موفقیت تغییر کرد.');
            history.push('/');
        }).catch((err) => {
            let message;
            try {
                message = err.response.data.error;
            } catch (error) {
                message = 'خطایی رخ داده است.';
            }

            toast.error(message);
        });
    }

    return (
        <div className='container'>
            <div className='change-password-body'>
                <form onSubmit={changePassword}>
                    <div className="mb-3">
                        <Input
                            value={password}
                            placeholder='رمز عبور'
                            type='password'
                            onChange={setPassword}
                        />
                    </div>

                    <button type="submit" className="btn btn-primary">تغییر رمز عبور</button>
                </form>
            </div>
        </div>
    );
}
