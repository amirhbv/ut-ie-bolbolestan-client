import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router';
import { toast } from 'react-toastify';

import './Login.css';
import { useAuth } from '../hooks/use-auth';
import Input from '../components/Input';

export default function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const history = useHistory();
    const auth = useAuth();

    function login(e) {
        e.preventDefault();

        auth.login(email, password).then(() => {
            toast.success('باموفقیت وارد شدید.');
            history.push('/');
        }).catch(() => {
            toast.error('ایمیل یا رمز عبور اشتباه است.');
        });
    }

    return (
        <div className='container'>
            <div className='login-body'>
                <form onSubmit={login}>
                    <div className="mb-3">
                        <Input
                            required
                            type="email"
                            value={email}
                            placeholder='ایمیل'
                            onChange={setEmail}
                        />
                    </div>
                    <div className="mb-3">
                        <Input
                            value={password}
                            placeholder='رمز عبور'
                            type='password'
                            onChange={setPassword}
                        />
                    </div>

                    <button type="submit" className="btn btn-primary">ورود</button>
                    <Link to="/forget-password">
                        <button className="btn btn-secondary ms-1">فراموشی رمز عبور</button>
                    </Link>
                </form>
            </div>
        </div>
    );
}
