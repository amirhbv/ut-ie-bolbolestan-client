import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { toast } from 'react-toastify';

import './Signup.css';
import { useAuth } from '../hooks/use-auth';
import Input from '../components/Input';

export default function Signup() {
    const [name, setName] = useState('');
    const [lastName, setLastName] = useState('');
    const [studentId, setStudentId] = useState('');
    const [birthDate, setBirthDate] = useState('');
    const [major, setMajor] = useState('');
    const [faculty, setFaculty] = useState('');
    const [level, setLevel] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const history = useHistory();
    const auth = useAuth();

    function signup(e) {
        e.preventDefault();

        auth.signup(name, lastName, studentId, birthDate, major, faculty, level, email, password).then(() => {
            toast.success('باموفقیت ثبت‌نام شدید.');
            history.push('/');
        }).catch((err) => {
            let message;
            try {
                message = err.response.data.error;
            } catch (error) {
                message = 'خطایی رخ داده است.';
            }

            toast.error(message);
        });
    }

    return (
        <div className='container'>
            <div className='signup-body'>
                <form onSubmit={signup}>
                    <div className="mb-3">
                        <Input
                            required
                            value={name}
                            placeholder='نام'
                            onChange={setName}
                        />
                    </div>

                    <div className="mb-3">
                        <Input
                            required
                            value={lastName}
                            placeholder='نام خانوادگی'
                            onChange={setLastName}
                        />
                    </div>

                    <div className="mb-3">
                        <Input
                            required
                            type="number"
                            value={studentId}
                            placeholder='شماره دانشجویی'
                            onChange={setStudentId}
                        />
                    </div>

                    <div className="mb-3">
                        <Input
                            required
                            type="date"
                            value={birthDate}
                            placeholder='تاریخ تولد'
                            onChange={setBirthDate}
                        />
                    </div>

                    <div className="mb-3">
                        <Input
                            required
                            value={major}
                            placeholder='رشته'
                            onChange={setMajor}
                        />
                    </div>

                    <div className="mb-3">
                        <Input
                            required
                            value={faculty}
                            placeholder='دانشکده'
                            onChange={setFaculty}
                        />
                    </div>

                    <div className="mb-3">
                        <Input
                            required
                            value={level}
                            placeholder='مقطع'
                            onChange={setLevel}
                        />
                    </div>

                    <div className="mb-3">
                        <Input
                            required
                            type="email"
                            value={email}
                            placeholder='ایمیل'
                            onChange={setEmail}
                        />
                    </div>

                    <div className="mb-3">
                        <Input
                            required
                            value={password}
                            placeholder='رمز عبور'
                            type='password'
                            onChange={setPassword}
                        />
                    </div>

                    <button type="submit" className="btn btn-primary">ثبت‌نام</button>
                </form>
            </div>
        </div>
    );
}
