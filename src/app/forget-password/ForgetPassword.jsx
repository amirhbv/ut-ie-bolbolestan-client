import React, { useState } from 'react';
import { toast } from 'react-toastify';

import './ForgetPassword.css';
import { useAuth } from '../hooks/use-auth';
import Input from '../components/Input';

export default function ForgetPassword() {
    const [email, setEmail] = useState('');

    const auth = useAuth();

    function forgetPassword(e) {
        e.preventDefault();

        auth.forgetPassword(email).then(() => {
            toast.success('ایمیل فراموشی رمز عبور با موفقیت ارسال شد.');
        }).catch((err) => {
            let message;
            try {
                message = err.response.data.error;
            } catch (error) {
                message = 'خطایی رخ داده است.';
            }

            toast.error(message);
        });
    }

    return (
        <div className='container'>
            <div className='forget-password-body'>
                <form onSubmit={forgetPassword}>
                    <div className="mb-3">
                        <Input
                            required
                            type="email"
                            value={email}
                            placeholder='ایمیل'
                            onChange={setEmail}
                        />
                    </div>

                    <button type="submit" className="btn btn-primary">ارسال ایمیل فراموشی</button>
                </form>
            </div>
        </div>
    );
}
