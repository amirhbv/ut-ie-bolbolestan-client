import React from 'react';
import PropTypes from 'prop-types';

import './Box.css';

function Box({ title, children }) {
    return (
        <div className="box-container">
            {
                title ?
                    <span className="box-title">
                        {title}
                    </span>
                    : ''
            }

            {children}
        </div>
    );
}

Box.propTypes = {
    title: PropTypes.string,
    children: PropTypes.node,
};

export default Box;
