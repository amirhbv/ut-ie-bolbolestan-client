import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from 'react-router-dom';
import PropTypes from 'prop-types';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import './App.css';
import Navbar from './navbar/Navbar';
import Footer from './footer/Footer';
import Login from './login/Login';
import Signup from './signup/Signup';
import ForgetPassword from './forget-password/ForgetPassword';
import ChangePassword from './change-password/ChangePassword';
import Home from './home/Home';
import Schedule from './schedule/Schedule';
import Courses from './courses/Courses';
import { ProvideAuth, useAuth } from './hooks/use-auth.js';


function PrivateRoute({ children, ...rest }) {
    const auth = useAuth();

    return (
        <Route
            {...rest}
            render={({ location }) =>
                auth.getIsLoggedIn() ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: '/login',
                            state: { from: location },
                        }}
                    />
                )
            }
        />
    );
}

PrivateRoute.propTypes = {
    children: PropTypes.node,
};

function PublicRoute({ children, ...rest }) {
    const auth = useAuth();

    return (
        <Route
            {...rest}
            render={({ location }) =>
                !auth.getIsLoggedIn() ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: '/',
                            state: { from: location },
                        }}
                    />
                )
            }
        />
    );
}

PublicRoute.propTypes = {
    children: PropTypes.node,
};


export default function App() {

    return (
        <ProvideAuth>
            <Router>
                <div className="body">
                    <Navbar />

                    <div className="content">
                        <Switch>
                            <PublicRoute path="/login">
                                <Login />
                            </PublicRoute>
                            <PublicRoute path="/signup">
                                <Signup />
                            </PublicRoute>
                            <PublicRoute path="/forget-password">
                                <ForgetPassword />
                            </PublicRoute>

                            <PrivateRoute path="/change-password">
                                <ChangePassword />
                            </PrivateRoute>

                            <PrivateRoute path="/schedule">
                                <Schedule />
                            </PrivateRoute>
                            <PrivateRoute path="/courses">
                                <Courses />
                            </PrivateRoute>
                            <PrivateRoute path="/">
                                <Home />
                            </PrivateRoute>
                        </Switch>
                    </div>

                    <Footer />
                </div>

                <ToastContainer />
            </Router>
        </ProvideAuth>
    );

}
