import React, { Fragment } from 'react';
import {
    NavLink,
} from 'react-router-dom';

import './Navbar.css';
import { useAuth } from '../hooks/use-auth';

export default function Navbar() {
    const auth = useAuth();

    return (
        <nav className="navbar sticky-top navbar-light bg-light">
            <div className="container">
                <a className="navbar-brand" href="/">
                    <img
                        src="/images/logo.png"
                        alt="logo"
                        width="41"
                        height="40"
                        className="d-inline-block"
                    />
                    بلبلستان
                </a>


                {
                    auth.getIsLoggedIn()
                        ? (
                            <Fragment>
                                <div className="navbar-nav flex-row">
                                    <NavLink exact className="nav-link me-3" to="/">خانه</NavLink>
                                    <NavLink exact className="nav-link me-3" to="/courses">انتخاب واحد</NavLink>
                                    <NavLink exact className="nav-link me-3" to="/schedule">برنامه هفتگی</NavLink>
                                </div>

                                <div role="button" className="navbar-logout" onClick={auth.logout}>
                                    <span className="navbar-logout-text"> خروج </span>
                                    <i className="flaticon-log-out navbar-logout-icon"></i>
                                </div>
                            </Fragment>
                        )
                        : (
                            <div className="navbar-nav flex-row">
                                <NavLink exact className="nav-link me-3" to="/login">ورود</NavLink>
                                <NavLink exact className="nav-link me-3" to="/signup">ثبت‌نام</NavLink>
                            </div>
                        )
                }
            </div>
        </nav>
    );

}
